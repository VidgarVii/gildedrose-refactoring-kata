require 'rspec'

require File.join(File.dirname(__FILE__), 'gilded_rose')

RSpec.describe GildedRose do

  KEYS = [[[9, 19], [1, 1], [4, 6], [0, 80], [-1, 80], [14, 21], [9, 50], [4, 50], [9, 48]],
    [[8, 18], [0, 2], [3, 5], [0, 80], [-1, 80], [13, 22], [8, 50], [3, 50], [8, 46]],
    [[7, 17], [-1, 4], [2, 4], [0, 80], [-1, 80], [12, 23], [7, 50], [2, 50], [7, 44]],
    [[6, 16], [-2, 6], [1, 3], [0, 80], [-1, 80], [11, 24], [6, 50], [1, 50], [6, 42]],
    [[5, 15], [-3, 8], [0, 2], [0, 80], [-1, 80], [10, 25], [5, 50], [0, 50], [5, 40]],
    [[4, 14], [-4, 10], [-1, 0], [0, 80], [-1, 80], [9, 27], [4, 50], [-1, 0], [4, 38]],
    [[3, 13], [-5, 12], [-2, 0], [0, 80], [-1, 80], [8, 29], [3, 50], [-2, 0], [3, 36]],
    [[2, 12], [-6, 14], [-3, 0], [0, 80], [-1, 80], [7, 31], [2, 50], [-3, 0], [2, 34]],
    [[1, 11], [-7, 16], [-4, 0], [0, 80], [-1, 80], [6, 33], [1, 50], [-4, 0], [1, 32]],
    [[0, 10], [-8, 18], [-5, 0], [0, 80], [-1, 80], [5, 35], [0, 50], [-5, 0], [0, 30]],
    [[-1, 8], [-9, 20], [-6, 0], [0, 80], [-1, 80], [4, 38], [-1, 0], [-6, 0], [-1, 26]]
  ]
  describe "#update_quality" do
    it "does not change the name" do
      items = [Item.new("foo", 0, 0)]
      GildedRose.new(items).update_quality()
      expect(items[0].name).to eq "foo"
    end

    it "11 days check all params " do
      items = [
        Item.new(name="+5 Dexterity Vest", sell_in=10, quality=20),
        Item.new(name="Aged Brie", sell_in=2, quality=0),
        Item.new(name="Elixir of the Mongoose", sell_in=5, quality=7),
        Item.new(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=70),
        Item.new(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
        Item.new(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
        Item.new(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
        Item.new(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
        # This Conjured item does not work properly yet
        Item.new(name="Conjured Mana Cake", sell_in=10, quality=50), # <-- :O
      ]

      11.times do |day|
        GildedRose.new(items).update_quality
        puts "День #{day+1}"

        items.each_with_index do |item, index|
          puts item.name
          expect(item.sell_in).to eq KEYS[day][index][0]
          expect(item.quality).to eq KEYS[day][index][1]
        end
      end
    end
  end

end
