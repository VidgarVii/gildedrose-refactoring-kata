class GildedRose

  def initialize items
    @items = items
  end

  def update_quality
    @items.each do |item|
      #Чтобы поставщик не обманул
      if item.quality > 50 && item.name != "Sulfuras, Hand of Ragnaros"
        item.quality = 50
      end

      item.sell_in -= 1 if item.name != "Sulfuras, Hand of Ragnaros"

      case item.name
      when "Conjured Mana Cake" then conjured item
      when "Sulfuras, Hand of Ragnaros" then sulfuras item
      when "Aged Brie" then aged_brie item
      when "Backstage passes to a TAFKAL80ETC concert" then backstage item
      else
        item.quality -= 1
        item.quality -= 1 if item.sell_in < 0
      end

      # Чтобы скрипт не сломал
      if item.quality > 50 && item.name != "Sulfuras, Hand of Ragnaros"
        item.quality = 50
      end

      item.quality = 0 if item.quality < 0 
    end
  end

  private

  def conjured item
    if item.sell_in >= 0 && item.quality > 0
      item.quality -= 2
    elsif item.sell_in < 0 && item.quality > 0
      item.quality -= 4
    end    
  end
  
  def sulfuras item
    item.quality = 80
  end
  
  def aged_brie item
    if item.sell_in >= 0
      item.quality += 1
    else
      item.quality += 2
    end
  end  

  def backstage item
    item.quality += 1
    
    case item.sell_in
    when 5..9 then item.quality += 1
    when 0..4 then item.quality += 2
    end
                   
    item.quality = 0 if item.sell_in < 0
  end  
end

class Item
  attr_accessor :name, :sell_in, :quality

  def initialize(name, sell_in, quality)
    @name = name
    @sell_in = sell_in
    @quality = quality
  end

  def to_s()
    "#{@name}, #{@sell_in}, #{@quality}"
  end
end