﻿# GildedRose-Refactoring-Kata – RUBY

This is a test task

Работа велась по принципу TDD

Возможно я не на 100% понял описание проекта, по этому после десятого раза прочтения и изучив код я выявил для себя ряд факторов:

1 Предоставленный код рабочий
2 Надо добавить новый функционал для нового товара
3 Возможен рефакторинг кода
4 Не трогать класс Item
5 Возможно не корректный перевод в описании


Как велась работа?

1 Написание 2х тестов (так как предлагалась использовать 2 библиотеки rspec & test/unit) 
	Для этого с помощью файла texttest_fixture с генерировал некую таблицу проверочных данных на 11 дней (посчитал что этого будет достаточно)
	
2 Рефакторинг и добавление нового функционала
	Я думаю в таких задачах лучше разделять условия и не городить вложенности и зависимости.
	Для каждого товара был создан новый частный метод, который обрабатывает все условия. Это позволит в будущем развивать приложение не ломая прежний функционал  для существующих товаров(категорий).    
